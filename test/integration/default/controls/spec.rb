id = attribute('id')
cidr = attribute('cidr')

control 'vpc_test' do
  impact 1
  
  describe aws_vpc(id) do
      it { should exist }
  end
end 