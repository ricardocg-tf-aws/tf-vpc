output "id" {
    value = module.test_vpc.vpc_id
}

output "cidr" {
    value = module.test_vpc.vpc_cidr
}