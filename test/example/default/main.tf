
module "test_vpc"{
    source                                  = "../../../"
    vpc_cidr                                = "10.0.0.0/16"
    env                                     = "prod"
}
