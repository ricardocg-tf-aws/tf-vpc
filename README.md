# AWS VPC MODULE 

- Deploys VPC in AWS for use cloud resources. 

# Inputs 

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| CIDR | CIDR FOR THE VPC i.e 10.0.0.0/16 | String | - |:yes:|
| Env | Env ehere the vpc should be deployed  | String | Test |:yes:|

# Outputs 

| Name | Description |
|------|-------------|
| vpc_cidr | CIDR  for the vpc|
| vpc_id | ID |

# Usage

```js
module "vpc" {
    source                          = "../../"
    cidr                            = "10.0.0.0/16"
    env                             = "test"
}
```