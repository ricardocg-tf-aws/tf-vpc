variable "vpc_cidr"{
    description = "CIDR FOR VPC"
    default = "10.0.0.0/16"
}

variable "env"{
    description = "env for vpc tags"
    default = "test"
}